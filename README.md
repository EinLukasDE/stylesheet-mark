**stylesheet-mark**

Mark element for older browsers, newer browsers can already read like this. Do not get older, the perfect solution is CSS.

---

## How to use

First, the mark.css file must be included in the header.

  <link href="mark.css" rel="stylesheet">

---

Next you can already use the mark.css.

  <p class="mark">Example text</p>


---

## Notice

A tutorial was written in the index.html, questions or concerns can be submitted in issues.

Usage without download <br>
  https://cdn.rawgit.com/EinLukasDE/stylesheet-mark/master/mark.css
  
GitHub: https://github.com/EinLukasDE/stylesheet-mark